*** Please add the app directory of this application to your PYTHONPATH ***

# EdgeTier Coding Challenge

This is an attempt of EdgeTier's "Location Code Test" coding challenge.

This application meets the requirements outlined in the original brief, with these additional features:

- Requests to the Meta Weather API have been reduced where possible to increase speed and reliability

- The application is written with scalability in mind, particularly developers should find it easy to add new API connections or change scoring logic

- All code is PEP8 compliant

- Robust exception handling is used, in no case has a *foreseen* exception been left unhandled (as none justify killing the program)


##Production Readiness

- Currently this application does not have logging, this would need to be added before it is production-ready

- Full input handling of user-controlled fields needs to be added before deploying to production


##Improvements

Some suggested enhancements to this application:

- Externalize configuration from the application

- Use a database for storing data, rather than the current use of a CSV file

- Account for cities with special characters (e.g. "Wrocław")
